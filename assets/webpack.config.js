const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CopyWebpackPlugin = require('copy-webpack-plugin')

const PRIV_STATIC_DIR = path.resolve(__dirname, '../priv/static')

const jsLoader = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
    },
}

const sassLoader = {
    test: /\.scss$/,
    use: [
        MiniCssExtractPlugin.loader, {
            loader: "css-loader",
            options: {
                url: false,
                sourceMap: true
            }
        }, {
            loader: "sass-loader",
            options: {
                sourceMap: true
            }
        }
    ]
}

const cssPlugin = new MiniCssExtractPlugin({
    filename: "css/app.css"
})

const copyPlugin = new CopyWebpackPlugin([{
    from: 'static/',
    to: './', // retative to config.output.path (priv/static)
    toType: 'dir',
    cache: true
}])

const production = process.env.NODE_ENV === 'production'

module.exports = {
    devtool: production ? 'source-maps' : 'eval-source-maps',
    entry: './js/index.js',
    output: {
        path: PRIV_STATIC_DIR,
        filename: 'js/app.js',
        publicPath: '/',
    },
    module: {
        rules: [jsLoader, sassLoader],
    },
    plugins: [cssPlugin, copyPlugin],
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'js')],
        extensions: ['.js'],
    },
}
