import {
  mapReducers
} from '../reducer-tools'
import {
  replace as navigate
} from 'connected-react-router'
import {
  update,
  set
} from '../util'
import {
  selfPlayerId,
} from '../game-init'
import {
  confirmPartyParticipation,
  submitPartyTurn,
} from './player'
import {
    EDGE_TYPE_VERTICAL,
    EDGE_TYPE_HORIZONTAL,
} from '../constants'


export function initialState() {
  return {}
}

export function setPartyState(party) {
  return {
    type: 'SET_PARTY_STATE',
    party,
  }
}

// updatePartyState is used to send an update function on a party to
// the store, instead of giving the full party state
export function updatePartyState(party_id, callback) {
  return {
    type: 'UPDATE_PARTY_STATE',
    party_id,
    callback
  }
}

export function cancelParty(party_id) {
  return dispatch => {
    setTimeout(function(){
      // @todo auto delete party ?
    }, 10 * 1000)
   return dispatch(setPartyState({ id: party_id, status: 'canceled' }))
  }
}

export function goToPartyView(id) {
  return dispatch => {
    dispatch(navigate(`/party/${id}`))
  }
}

export function confirmPlay(party_id) {
  return dispatch => {
    dispatch(updatePartyState(party_id, party => partyWithPlayerConfirmed(party, selfPlayerId)))
    dispatch(confirmPartyParticipation(party_id))
  }
}

export function sendTurnFromEdge(party_id, type, x, y) {
  if (type !== EDGE_TYPE_HORIZONTAL && type !== EDGE_TYPE_VERTICAL) {
    throw new Error("@todo")
  }
  const turn = {
    edge_type: type,
    x,
    y,
  }
  return submitPartyTurn(party_id, turn)
}

export const reducer = mapReducers(function(reduce) {
  reduce('SET_PARTY_STATE', (state, {
    party
  }) => Object.assign(state, {
    [party.id]: {...party,
      updatedAt: Date.now()
    }
  }))
  reduce ('UPDATE_PARTY_STATE', (parties, action) => {
    const party = action.callback(parties[action.party_id])
    return { ...parties, [action.party_id]: party }
  })
})

function partyWithPlayerConfirmed(party, player_id) {
  const players_meta = party.players_meta
    .map(pmeta => {
      if (pmeta.player_id === player_id) {
        return {...pmeta,
          confirmed_play: true
        }
      } else {
        return pmeta
      }
    })
    return { ...party, players_meta }
}
