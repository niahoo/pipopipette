import {
  Presence
} from "phoenix"
import {
  update,
  pretty,
  append
} from "../util"
import {
  mapReducers
} from '../reducer-tools'
import {
  join,
  push
} from '../socket'
import {
  setPartyState,
  cancelParty,
  goToPartyView,
} from './parties'
import {
  selfPlayerId
} from "game-init"

export function initialState() {
  return {}
}

function playerTopic(player_id) {
  return `player:${player_id}`
}

const SELF_TOPIC = playerTopic(selfPlayerId)

export function joinPrivateChannel() {
  return dispatch => {
    join(SELF_TOPIC, {
      dispatch,
      onMessage: function onMessage(evt, payload) {
        console.log('player msg : event: %s ; payload: ', evt, payload)
        return payload
      },
      events: {
        party_update({ party }) {
          dispatch(setPartyState(party))
        },
        party_canceled({ party_id }) {
          dispatch(cancelParty(party_id))
        }
      }
    })
  }
}

export function requestParty(oponent_id) {
  return dispatch => {
    dispatch(goToPartyView('loading'))
    const payload = {
      oponent_id
    }
    push(SELF_TOPIC, 'party_create', payload, {
      ok(response) {
        const { party_id } = response
        dispatch(goToPartyView(party_id))

      }
    })
  }
}

export function confirmPartyParticipation(party_id) {
  return dispatch => {
    const payload = {
      party_id
    }
    console.error("@todo ! confirm on server")
    push(SELF_TOPIC, 'party_accept', payload)
  }
}

export function subscribeToParty(party_id) {
  throw new Error("deprecated")
  return dispatch => {
    const payload = {
      party_id
    }
    push(SELF_TOPIC, 'party_subscribe', payload)
  }
}

export function submitPartyTurn(party_id, turn) {
    console.log('submitPartyTurn party_id', party_id)
    console.log('submitPartyTurn turn', turn)
    const payload = {
      party_id,
      turn
    }
    return dispatch => {
      push(SELF_TOPIC, 'party_turn', payload, {
        ok(response) {
          console.log('submitted turn ok : ', response)
        }
      })
    }
}

export const reducer = mapReducers(function(reduce) {
  reduce('LOBBY_PLAYER_LEFT', (state, _) => state)
})
