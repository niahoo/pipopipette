import {
  Presence
} from "phoenix"
import {
  update,
  pretty,
  append
} from "../util"
import {
  mapReducers
} from '../reducer-tools'
import {
  join,
  push
} from '../socket'
export function initialState() {
  return {
    presence: {},
    log: []
  }
}

function presenceState(data) {
  return (dispatch, getState) => {
    let {
      presence
    } = getState().lobby
    presence = Presence.syncState(presence, data)
    dispatch(setPresence(presence))
  }
}

function presenceDiff(data) {
  return (dispatch, getState) => {
    let {
      presence
    } = getState().lobby
    presence = Presence.syncDiff(presence, data)
    dispatch(setPresence(presence))
  }
}

function otherPlayerJoinedLobby(player) {
  return {
    type: 'LOBBY_OTHER_PLAYER_JOINED',
    player
  }
}

function playerLeft(player) {
  return {
    type: 'LOBBY_OTHER_PLAYER_LEFT',
    player
  }
}
export function joinLobby() {
  return dispatch => {
    join('lobby:main', {
      // onMessage: function onMessage(evt, payload) {
      //   console.log('lobby msg : %s', evt, payload)
      //   return payload
      // },
      // ok(resp) { console.debug('join lobby ok', resp) },
      // error(resp) { console.error('join lobby error', resp) },
      events: {
        presence_state(state) {
          dispatch(presenceState(state))
        },
        presence_diff(diff) {
          dispatch(presenceDiff(diff))
          Object.entries(diff.joins).forEach(([_, join]) => {
            dispatch(otherPlayerJoinedLobby(join.player))
          })
          Object.entries(diff.leaves).forEach(([_, leave]) => {
            dispatch(playerLeft(leave.player))
          })
        },
      }
    })
  }
}

function setPresence(presence) {
  return {
    type: 'SET_PRESENCE',
    presence
  }
}

export const reducer = mapReducers(function(reduce) {
  reduce('SET_PRESENCE', (state, action)  => {
    return { ...state, presence: action.presence }
  })
  reduce('LOBBY_OTHER_PLAYER_LEFT', (state, action) => {
    const log= state.log.concat("Player " + action.player.nickname + " left")
    return { ...state, log }
  })
  reduce('LOBBY_OTHER_PLAYER_JOINED', (state, action) => {
    const log= state.log.concat("Player " + action.player.nickname + " joined")
    return { ...state, log }
  })
})
