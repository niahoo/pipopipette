import { initialState as lobby } from './plugs/lobby'
import { initialState as player } from './plugs/player'
import { initialState as parties } from './plugs/parties'
export default {
  lobby: lobby(),
  player: player(),
  parties: parties(),
}
