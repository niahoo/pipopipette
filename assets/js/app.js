import { makeStore } from './store'
import { joinLobby } from './plugs/lobby'
import { joinPrivateChannel } from './plugs/player'
import { createAppView } from './views/AppView'

import {
  createBrowserHistory
} from 'history'

const history = createBrowserHistory()
const store = makeStore({ history })
setTimeout(() => store.dispatch(joinLobby()), 1)
setTimeout(() => store.dispatch(joinPrivateChannel()), 1)
const container = document.getElementById('app')
createAppView({ store, history, container })
