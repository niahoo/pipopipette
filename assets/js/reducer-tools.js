export function composeReducers() {
  const reducers = (arguments.length === 1 && Array.isArray(arguments[0]))
    ? arguments[0]
    : Array.from(arguments)
  if (reducers.length === 0) {
    return arg => arg
  }
  if (reducers.length === 1) {
    return reducers[0]
  }
  return (state, action) => reducers.reduce((state, f) => {
    return f(state, action)
  }, state)
}

const castArray = term => Array.isArray(term) ? term : [].concat(term)

export function registryToReducer(registry) {
  // We receive a map of actionType => [reducer]
  // We return a reducer (state, action) => state
  // For each actionType, we have an array and we want a function
  const composedByType = Object.entries(registry).reduce(
    (acc, [type, fns]) => Object.assign(acc, {[type]: composeReducers(fns)})
  , {})
  const reducer = (state = {}, action) => {
    const forType = composedByType[action.type]
    return forType ? forType(state, action) : state
  }
  reducer.handledActionTypes = Object.keys(registry)
  return reducer
}

function checkReducerValidity(reducerFn, actionType) {
  if (reducerFn.length !== 2) {
    console.warn(`A reducer should accept 2 arguments, state and action.
action type: ${actionType}
reducer: ${reducerFn.toString()}`)
  }
}

export function mapReducers(registrator) {
  const registry = {}
  const registerReducersForTypes = (actionTypes, reducerFns) => {
    actionTypes = castArray(actionTypes)
    reducerFns = castArray(reducerFns)
    actionTypes.forEach(actionType => {
      reducerFns.forEach(reducerFn => {
        checkReducerValidity(reducerFn, actionType)
        const arr = (registry[actionType] = registry[actionType] || [])
        arr.push(reducerFn)
      })
    })
  }
  registrator(registerReducersForTypes)
  const handledActionTypes = Object.keys(registry)
  const reducer = registryToReducer(registry)
  return reducer
}
