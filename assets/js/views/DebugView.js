import { connect } from 'react-redux'
import { sortBy } from '../util'
import { selfPlayerId } from "game-init"
import { requestParty } from '../plugs/lobby'

function View(props) {
    return (<pre>
        {JSON.stringify(props.debug, 0, '  ')}
    </pre>)
}

function mapStateToProps(state) {
    const { parties } = state
    const debug = { parties }
    // const debug = state
    return { debug }
}

export default connect(
    mapStateToProps,
    { requestParty },
)(View)
