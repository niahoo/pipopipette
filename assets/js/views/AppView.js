import { Route, Switch } from 'react-router'
import { ConnectedRouter } from 'connected-react-router'
import DebugView from './DebugView'
import LobbyView from './LobbyView'
import PartyView from './PartyView'
import PartiesListView from './PartiesListView'
import LoadingView from './LoadingView'
import HomeView from './HomeView'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

export function createAppView({ store, history, container }) {

render(
  <Provider store={store}>
    <div>
      <a href="/">home</a> | <a href="/debug/dropdb">drop db</a>
      <LobbyView/>
      <PartiesListView/>
    	<ConnectedRouter history={history}>
    		<Switch>
    			<Route exact path="/" component={HomeView} />
          <Route exact path="/party/loading" component={LoadingView} />
    			<Route path="/party/:partyId" component={PartyView} />
    		</Switch>
    	</ConnectedRouter>
    	<DebugView/>
    </div>
  </Provider>,
  container
)

}
