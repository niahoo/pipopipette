import { connect } from 'react-redux'
import { sortBy } from '../util'
import { selfPlayerId } from "game-init"
import { requestParty } from '../plugs/player'

function View(props) {

    function renderPlayer(player, props) {
        const you = player.id === selfPlayerId
        const clickPlayer = you ? null : () => props.requestParty(player.id)
        return (
            <li key={player.id}>{
                you
                ?   <span className="lobby-player-self">
                        {player.nickname} ({player.id}){you?' (you)':null}
                    </span>
                :   <a onClick={clickPlayer}>
                        {player.nickname} ({player.id}){you?' (you)':null}
                    </a>
            }</li>
        )
    }

    return (
        <div>
            <h2>Players</h2>
            <ul>
                { props.players.map(player => renderPlayer(player, props)) }
            </ul>
        </div>
    )
}

function mapStateToProps(state) {
    const { presence } = state.lobby
    const players = Object.keys(presence).map(function(k) {
        return presence[k].player
    })
    players.sort(sortBy('nickname'))
    return { players }
}

export default connect(
    mapStateToProps,
    { requestParty },
)(View)
