import {
  connect
} from 'react-redux'
import { selfPlayerId } from "game-init"
import {
  pretty
} from '../util'
import {
  confirmPlay
} from '../plugs/parties'
import BoardView from './BoardView.js'
import { bindActionCreators } from 'redux'

console.warn("@todo button refuse party")

function playerNickname(party, playerId) {
  return party.players_meta.find(meta => meta.player_id === playerId).nickname
}

class View extends React.Component {
  render() {
    console.warn('render party', this.props)
    const {
      party
    } = this.props
    if (party === null) {
        return this.renderNoParty()
    }
    switch (party.status) {
      case 'awaiting_confirm':
        return this.renderAwaiting(party)
      case 'canceled':
        return this.renderCanceled()
      case 'awaiting_turn':
      case 'finished':
        return this.renderPartyBoard(party)
      default:
        throw new Error(`Unknown party status ${party.status}`)
    }
  }
  renderNoParty() {
    return (<div style={{color:'red'}}>No such party</div>)
  }
  renderLoading(party) {
    return (<div style={{color:'blue'}}>Loading ...</div>)
  }
  renderCanceled() {
    return (<div style={{color:'red'}}>
      Party canceled !
      @todo button "ok" (cleanup state)
    </div>)
  }
  renderAwaiting(party) {
    const confirmations = party.players_meta.map(pmeta => this.renderPlayerConfirmInfo(pmeta))

    return (<div>
        <h2>Party</h2>
        <p>Party is awaiting players</p>
        <ul>{confirmations}</ul>
        <pre>{pretty(party)}</pre>
    </div>)
  }
  renderPlayerConfirmInfo(playerMeta) {
    console.log('this.props.confirmPlay', this.props.confirmPlay.toString())
    const statusEl =
      // player accepted
        playerMeta.confirmed_play ? 'OK'
      // player not accepted and is self
        : playerMeta.player_id === selfPlayerId
        ? <a className="button" onClick={this.props.confirmPlay}>Play !</a>
      // player not accepted and is somebody else
        : 'Awaiting ...'

    return <li key={playerMeta.player_id}>{playerMeta.nickname} : {statusEl}</li>
  }
  renderPartyBoard(party) {
    let turn
    if (party.status === 'finished') {
      turn = this.renderVictory()
    } else {
      turn = this.renderTurn()
    }
    return (<div>
      <h2>Party</h2>
      {turn}
      <BoardView party={party}/>
    </div>)
  }
  renderVictory() {
    const { party } = this.props
    const { winner } = party
    const winnerText = winner === selfPlayerId
      ? "Vous avez gagné !"
      : "%s a gagné :(".replace('%s', playerNickname(party, winner))
    return <span style={{color:'red'}}>{winnerText}</span>
  }
  renderTurn() {
    const yourTurnTxt = "C'est votre tour !"
    const otherTurnTxt = "À %s de jouer"
    const { party } = this.props
    const { player_turn } = party
    if (player_turn === selfPlayerId) {
        return <span style={{color:'red'}}>{yourTurnTxt}</span>
    } else {
        console.log('player turn : %s', player_turn)
        console.log('party.players_meta', party.players_meta)
        const otherNick = playerNickname(party, player_turn)
        return <span>{otherTurnTxt.replace('%s', otherNick)}</span>
    }
  }
}

function mapStateToProps(state, ownProps) {
  const {
    partyId
  } = ownProps.match.params
  const party = state.parties[partyId] || null
  const props = {
    party
  }
  return props
}

function mapDispatchToProps(dispatch, ownProps) {
  const {
    partyId
  } = ownProps.match.params
  return {
    confirmPlay: playerId => dispatch(confirmPlay(partyId))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
)(View)
