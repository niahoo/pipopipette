import {
  connect
} from 'react-redux'
import { selfPlayerId } from "game-init"
import {
  pretty
} from '../util'
import {
  goToPartyView
} from '../plugs/parties'


class View extends React.Component {
  render() {
    const {
      parties,updatedAt
    } = this.props
    return (
      <div>
        <h2>Parties</h2>
        <div>lastup : {pretty(updatedAt)}</div>
        <ul>
          {parties.map(p => this.renderParty(p))}
        </ul>
      </div>
      )
  }
  renderParty(party) {
    const id = party.id.split('-')[0]
    const navigate = evt => {
      this.props.goToPartyView(party.id)
    }
    return (<li key={id}>
      <a onClick={navigate}>{id}</a>
      {' '}
      {party.status}
    </li>)
  }
}

function mapStateToProps(state) {
  let maxUpdate = 0
  const parties = Object.entries(state.parties).map(
    ([_, party]) => {
      maxUpdate = Math.max(party.updatedAt, maxUpdate)
      return party
    }
  )
  const props = {
    parties: parties,
    updatedAt: maxUpdate
  }
  return props
}


function mapDispatchToProps(dispatch) {
  return {
    goToPartyView: partyId => dispatch(goToPartyView(partyId))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
)(View)
