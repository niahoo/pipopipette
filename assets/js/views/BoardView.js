import {
  connect
} from 'react-redux'
import { selfPlayerId } from "game-init"
import {
  pretty
} from '../util'
import { bindActionCreators } from 'redux'
import { sendTurnFromEdge } from '../plugs/parties'
import {
    EDGE_TYPE_VERTICAL,
    EDGE_TYPE_HORIZONTAL,
} from '../constants'

const flattenProto = require('array.prototype.flat')
flattenProto.shim()

const BOX_SIZE = 40
const BOX_HALF = Math.round(BOX_SIZE / 2)
const BLANK_EDGE = 0
const EDGE_WIDTH = 3
const CIRCLE_RAY = 4


function translate(x, y) {
    return 'translate(x y)'
        .replace('x', x)
        .replace('y', y)
}

const COLOR_GREY = 'rgb(100,100,100)'
const COLOR_GREY_LIGHT = 'rgb(200,200,200)'
const COLOR_BLUE = 'rgb(0,0,255)'
const COLOR_RED = 'rgb(255,0,0)'

function generateDots(board) {
    const nestedCirclesCoords = [...Array(board.rows + 1).keys()]
        .map(cx => [...Array(board.cols + 1).keys()].map(cy => ({ cx, cy })))
    return Array.prototype.concat.apply([], nestedCirclesCoords)
        .map(({ cx, cy }) => {
            const transform = translate(cx * BOX_SIZE, cy * BOX_SIZE)
            return <circle
                key={[cx, cy].join()}
                fill={COLOR_GREY_LIGHT}
                cx="0" cy="0" r={CIRCLE_RAY}
                transform={transform}
                />
        })
}

function generateEdges(board) {
    const verticals = board.edgesVert.map((edges, x) => edges.map((content, y) => {
        return genEdge(content, x, y, EDGE_TYPE_VERTICAL)
    }))
    const horizontals = board.edgesHoriz.map((edges, x) => edges.map((content, y) => {
        return genEdge(content, x, y, EDGE_TYPE_HORIZONTAL)
    }))
    console.log('verticals', verticals)
    return [verticals,horizontals].flat()
}

function genEdge(content, x, y, type) {
    const transform = translate(x * BOX_SIZE, y * BOX_SIZE)
    const color = colorOf(content)
    const x2 = type === EDGE_TYPE_HORIZONTAL ? BOX_SIZE : 0
    const y2 = type === EDGE_TYPE_VERTICAL ? BOX_SIZE : 0
    const key = encodeEdgeInfo(type, x, y)
    return <line key={key}
            data-edge={key}
            x1="0" y1="0" x2={x2} y2={y2}
            style={{stroke: color, strokeWidth: EDGE_WIDTH, cursor: 'pointer' }}
            transform={transform} />
}

function generateTexts(board, nicknames) {
    return board.boxes.map((col, x) => col.map((box, y) => {
        const key = ['txt', x, y].join()
        const color = colorOf(box)
        const transform = translate(x * BOX_SIZE, y * BOX_SIZE)
        const nick = nicknames[box]
        return <text key={key}
            alignmentBaseline="central"
            x={BOX_HALF} y={BOX_HALF}
            style={{fill: color}}
            transform={transform}>
            {nick}
            </text>
    }))
    .flat()

}

function colorOf(content) {
    return content === BLANK_EDGE
        ? COLOR_GREY
        : content === selfPlayerId
        ? COLOR_BLUE
        : COLOR_RED
}
const EDGE_INFO_SEP = '-'
function decodeEdgeInfo(str) {
    const [type, x, y] = str.split(EDGE_INFO_SEP)
    return [type, parseInt(x), parseInt(y)]
}

function encodeEdgeInfo(type, x, y) {
    return [type, x, y].join(EDGE_INFO_SEP)
}


function ids2nicknames(party) {
    const map = {}
    party.players_meta.forEach(({ player_id, nickname }) => map[player_id] = nickname.slice(0,3))
    return map
}

class View extends React.Component {
  render() {
    const graph = this.renderGraph()
    return <div>
        {graph}
        </div>
  }
  renderGraph() {
    const { party } = this.props
    const { board } = party

    const config = {
        margin: {top: 20, left: 20, right: 20, bottom: 20},
        height: BOX_SIZE * board.rows,
        width: BOX_SIZE * board.cols,
    }

    const graphHeight = config.height
    const graphWidth = config.width

    const svgHeight = config.height + config.margin.top + config.margin.bottom
    const svgWidth = config.width + config.margin.left + config.margin.right

    const paperTransform =  translate(config.margin.left, config.margin.top)
    console.log('paperTransform', paperTransform)
    const dots = generateDots(board)
    console.log('dots', dots)
    const clickEdge = (evt) => {
        if (evt.target.nodeName !== 'line') { throw new Error("@todo") }
        const [type, x, y] = decodeEdgeInfo(evt.target.getAttribute('data-edge'))
        console.log('clicked on type:%s ', type, x, y)
        this.props.onEdgeClick(type, x, y)
    }
    const edges = generateEdges(board)
    console.log('edges', edges)
    const texts = generateTexts(board, ids2nicknames(party))
    console.log('texts', texts)
    return (
    <svg height={svgHeight} width={svgWidth} className="pipo-board">
        <g transform={paperTransform}>
            <rect fill="white" x="0" y="0" width={graphWidth + 'px'} height={graphHeight + 'px'}/>
            <g onClick={clickEdge}>
                {edges}
            </g>
            <g id="dots">{dots}</g>
            <g id="texts">{texts}</g>
        </g>
    </svg>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {}
}

function mapDispatchToProps(dispatch, ownProps) {
  const partyId = ownProps.party.id
  return {
    onEdgeClick: (type, x, y) => dispatch(sendTurnFromEdge(partyId, type, x, y))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
)(View)
