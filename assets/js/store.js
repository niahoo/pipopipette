import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import { diff } from 'deep-diff'
import initialState from './initial-state'
import thunk from 'redux-thunk'
import {
  combineReducers
} from 'redux'
import {
  reducer as partiesReducer
} from './plugs/parties'
import {
  reducer as lobbyReducer
} from './plugs/lobby'
import {
  reducer as playerReducer
} from './plugs/player'
import {
  composeReducers
} from './reducer-tools'
import {
  connectRouter,
  routerMiddleware
} from 'connected-react-router'

export function makeStore({ history}) {

  let reducer = composeReducers(
    combineReducers({
      lobby: lobbyReducer,
      player: playerReducer,
      parties: partiesReducer,
    }),
    (function() {
      let prevState
      return function(state, action) {
        const difference = diff(prevState, state)
        if (difference === void 0) {
          console.log('Action had no effect : %s', action.type, action)
        } else {
          prevState = state
        }
        return state
      }
    }()))

  reducer = connectRouter(history)(reducer)

  const store = createStore(
    reducer,
    initialState,
    applyMiddleware(
      routerMiddleware(history),
      thunk
    )
  )
  return store
}
