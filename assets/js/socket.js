import {
  Socket
} from "phoenix"
import {
  selfPlayerId
} from "game-init"
import {
  isFunction,
  isObject,
  inspect,
  pretty,
} from "./util"
const socket = new Socket("/ws", {
  params: {
    player_id: selfPlayerId
  }
})
socket.onError(err => {
  console.log("socket error")
  console.error(err)
})
socket.connect()
const channels = {}

function channel(topic) {
  if (channels[topic]) return channels[topic]
  else throw new Error("Channel " + topic + " is not joined")
}

function registerChannel(channel, topic) {
  channels[topic] = channel
}

function clearChannel(topic) {
  delete channels[topic]
}

export function join(topic, opts = {}) {
  if (channels[topic]) {
    throw new Error("Already joined " + topic.toString())
  }
  const channel = socket.channel(topic, opts.params)
  if (isFunction(opts.onMessage)) {
    channel.onMessage = opts.onMessage
  }
  if (isObject(opts.events)) {
    Object.entries(opts.events).forEach(([event, handler]) => {
      channel.on(event, handler)
    })
  }
  const joinSuccess = isFunction(opts.ok) ?
    opts.ok :
    bindDispatchForJoinSuccess(topic, opts.dispatch || warnHunhandled)
  const joinError = isFunction(opts.error) ?
    opts.error :
    bindDispatchForJoinError(topic, opts.dispatch || warnHunhandled)
  channel.onClose(info => console.log('channel closed', info))
  channel.onClose(() => clearChannel(topic))
  channel.onError(err => console.error('channel error', err))
  channel.join()
    .receive('ok', joinSuccess)
    // register channel will be called after each channel process
    // server-side crash. The registration is overriden but the
    // channel objects remains the same
    .receive('ok', () => registerChannel(channel, topic))
    .receive('error', joinError)
}

export function push(topic, event, payload, opts = {}) {
  const pushSuccess = isFunction(opts.ok) ?
    opts.ok :
    bindDispatchForPushSuccess(topic, event, opts.dispatch || warnHunhandled)
  const pushError = isFunction(opts.error) ?
    opts.error :
    bindDispatchForPushError(topic, event, opts.dispatch || warnHunhandled)
  channel(topic)
    .push(event, payload)
    .receive('ok', pushSuccess)
    .receive('error', pushError)
}

export default socket

function warnHunhandled(action) {
  const {
    event,
    response,
    status,
    topic,
    type,
  } = action
  console.warn(`Unhandled server message
type      ${type}
event     ${event}
status    ${status}
topic     ${topic}
response  ${pretty(response)}
`)
}

function bindDispatchForJoinSuccess(topic, dispatch) {
  return response => dispatch({
    topic,
    event: '__join__',
    status: 'ok',
    response,
    type: 'JOIN_CHANNEL_OK'
  })
}

function bindDispatchForJoinError(topic, dispatch) {
  return response => dispatch({
    topic,
    event: '__join__',
    status: 'error',
    response,
    type: 'JOIN_CHANNEL_ERROR'
  })
}

function bindDispatchForPushSuccess(topic, event, dispatch) {
  return response => dispatch({
    topic,
    event,
    status: 'ok',
    response,
    type: 'PUSH_CHANNEL_OK'
  })
}

function bindDispatchForPushError(topic, event, dispatch) {
  return response => dispatch({
    topic,
    event,
    status: 'error',
    response,
    type: 'PUSH_CHANNEL_ERROR'
  })
}
