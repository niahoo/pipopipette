// Set React globally for it to work as a JSX pragma
import React from 'react'
window.React = React

require('./app.js')

require('../scss/app.scss')
