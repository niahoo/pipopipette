export const get = (prop) => state => state[prop]
export const getIn = (...path) => state => {
  return getInNoCurry(state, path)
}

const getInPath = (state, path) => {
  let i, l = path.length
    // as soon as state is null or undef, we stop
  for (i = 0; state != null && i < l; i++) {
    state = state[path[i]];
  }
  // if we did not use all our path, return undef
  return (i && i == l) ? state : undefined;
}

const setInPath = (state, path, value) => {
  const head = path[0]
  if (path.length === 1) {
    return {...state,
      [head]: value
    }
  } else if (path.length === 0) {
    throw new Error("Path must be a non-empty array")
  }
  const tail = path.slice(1)
  return {...state,
    [head]: setInPath(state[head], tail, value)
  }
}

export function update() {
  throw new Error("deprecated")
}
export const EXupdate = (prop, fn, opts = {}) => (state, action) => {
  const propIsPath = Array.isArray(prop)
  let prev = propIsPath ?
    getInPath(state, prop)[prop] :
    state[prop]
  if (prev === void 0 && opts.create) {
    prev = opts.create()
  }
  const newState = propIsPath ?
    setInPath(state, prop, fn(prev, action)) : {...state,
      [prop]: fn(prev, action)
    }
  return newState
}

export const append = (prop, fn) => update(prop, (array, action) => array.concat(fn(action)))
  // export const cons = (prop, value) => update(prop, (array) => [value].concat(array))

export const set = (prop, value) => (state, action) => {
  // if a function is provided, we call it with the action. if it is
  // another term, we just set the value. As a function is not
  // serializable and redux state should be only plain data, it's
  // fine.
  return {...state,
    [prop]: typeof value === 'function' ? value(action) : value
  }
}

export function simpleSorter(a, b) {
  return a < b ? -1 : a > b ? 1 : 0
}

export function sort(array, comparator = simpleSorter) {
  if (array.asMutable) {
    array = array.asMutable()
  } else {
    array = array.slice()
  }
  return array.sort(comparator)
}

export function sortBy(fn) {
  if (typeof fn === 'string') {
    return function(a, b) {
      return simpleSorter(a[fn], b[fn])
    }
  } else {
    return function(a, b) {
      return simpleSorter(fn(a), fn(b))
    }
  }
}

export function isFunction(f) {
  return typeof f === 'function'
}

export function isObject(f) {
  return typeof f === 'object'
}

export function inspect(term) {
  return JSON.stringify(term)
}

export function pretty(term) {
  return JSON.stringify(term, 0, '  ')
}
