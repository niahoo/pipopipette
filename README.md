# Pipopipette

### Install

  * `mix deps.get`
  * `mkdir db-dev`
  * `cd assets && yarn`

### Run

  * `mix phx.server`

Visit [`localhost:4000`](http://localhost:4000) in two different browsers.

