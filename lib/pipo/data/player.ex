# Players are stored in a public ETS table

defmodule Pipo.Data.Player do
  defstruct [:id, :nickname]
end
