defmodule Pipo.Data.Board do
  defstruct [:rows, :cols, :edgesHoriz, :edgesVert, :boxes]

  @edgeHoriz "h"
  @edgeVert "v"
  @blankValue 0

  def new(rows, cols) do
    # Si on a un tableau de 2x2 on à 3 rangées de lignes horizontales et 3
    # rangées de lignes verticales).

    # Pour chaque rangée de lignes horizontales on construit une liste avec
    # :blank pour chaque colonne
    edgesHoriz =
      0..(cols - 1)
      |> Enum.map(fn _x -> List.duplicate(@blankValue, _y = rows + 1) end)

    # Idem pour les colonnes, on garde l'ordre row/col
    edgesVert =
      0..cols
      |> Enum.map(fn _x -> List.duplicate(@blankValue, _y = rows) end)

    # As we use JSON for the client, we will use nested listes for the boxes
    # too, instead of a 2-tuple {x,y} key
    # boxes =
    #   for _x <- 0..(cols - 1) do
    #     for _y <- 0..(rows - 1) do
    #       @blankValue
    #     end
    #   end
    boxes = List.duplicate(List.duplicate(@blankValue, rows), cols)

    %__MODULE__{
      rows: rows,
      cols: cols,
      edgesHoriz: edgesHoriz,
      edgesVert: edgesVert,
      boxes: boxes
    }
  end

  def blank_edge?(board, edge) do
    board
    |> get_edge(edge)
    |> Kernel.===(@blankValue)
  end

  defp get_edge(%__MODULE__{} = board, %{edge_type: edge_type, x: x, y: y}) do
    Map.get(board, edge_group(edge_type))
    |> list_at(x)
    |> list_at(y)
  end

  defp blank_box?(board, box) do
    board
    |> get_box(box)
    |> Kernel.===(@blankValue)
  end

  defp get_box(%__MODULE__{} = board, {x, y}) do
    board.boxes
    |> list_at(x)
    |> list_at(y)
  end

  defp set_box(%__MODULE__{} = board, {x, y}, content) do
    Map.update!(board, :boxes, fn cols ->
      List.update_at(cols, x, fn row ->
        List.replace_at(row, y, content)
      end)
    end)
  end

  defp box_surrounded?(%__MODULE__{} = board, {x, y}) do
    [
      get_edge(board, %{edge_type: @edgeHoriz, x: x, y: y}),
      get_edge(board, %{edge_type: @edgeHoriz, x: x, y: y + 1}),
      get_edge(board, %{edge_type: @edgeVert, x: x, y: y}),
      get_edge(board, %{edge_type: @edgeVert, x: x + 1, y: y})
    ]
    |> Enum.all?(&(&1 !== @blankValue))
  end

  def set_edge(%__MODULE__{} = board, %{edge_type: edge_type, x: x, y: y} = edge, content) do
    board
    |> Map.update!(edge_group(edge_type), fn cols ->
      List.update_at(cols, x, fn row -> List.replace_at(row, y, content) end)
    end)
  end

  def maybe_set_boxes(%__MODULE__{} = board, edge, content) do
    boxes_to_fill =
      board
      |> get_edge_boxes_coords(edge)
      |> Enum.filter(fn coords ->
        blank_box?(board, coords) and box_surrounded?(board, coords)
      end)

    IO.puts("boxes to fill : #{inspect(boxes_to_fill)}")

    if length(boxes_to_fill) > 0 do
      new_board =
        Enum.reduce(boxes_to_fill, board, fn box, board ->
          set_box(board, box, content)
        end)

      {:changed, new_board}
    else
      {:unchanged, board}
    end
  end

  @todo "Match on functions instead of a case"
  defp get_edge_boxes_coords(%__MODULE__{rows: rows, cols: cols}, %{edge_type: type, x: x, y: y}) do
    case {type, x, y} do
      {@edgeHoriz, _, 0} -> [{x, y}]
      {@edgeHoriz, _, ^rows} -> [{x, y - 1}]
      {@edgeHoriz, _, _} -> [{x, y - 1}, {x, y}]
      {@edgeVert, 0, _} -> [{x, y}]
      {@edgeVert, ^rows, _} -> [{x - 1, y}]
      {@edgeVert, _, _} -> [{x - 1, y}, {x, y}]
    end
  end

  def validate_edge(%__MODULE__{rows: rs, cols: cs}, %{edge_type: @edgeHoriz, x: x, y: y})
      when x <= cs and y <= rs + 1 do
    :ok
  end

  def validate_edge(%__MODULE__{rows: rs, cols: cs}, %{edge_type: @edgeVert, x: x, y: y})
      when x <= cs + 1 and y <= rs do
    :ok
  end

  def validate_edge(_, edge), do: {:error, :invalid_edge}

  def check_victory(board) do
    scores = compute_scores(board)
    IO.puts("scores: #{inspect(scores)}")

    case scores do
      %{@blankValue => n} when n > 0 ->
        :unfinished

      scores ->
        {winner, _score} = Enum.max(scores)
        {:winner, winner}
    end
  end

  defp compute_scores(board) do
    board.boxes
    |> Enum.reduce(%{}, fn col, acc ->
      Enum.reduce(col, acc, fn content, acc ->
        Map.update(acc, content, 1, &(&1 + 1))
      end)
    end)
  end

  defp edge_group(@edgeHoriz), do: :edgesHoriz
  defp edge_group(@edgeVert), do: :edgesVert

  defp list_at(list, n), do: :lists.nth(n + 1, list)
end
