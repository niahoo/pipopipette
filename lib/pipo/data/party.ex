defmodule Pipo.Data.Party.PlayerMeta do
  defstruct [:player_id, :confirmed_play, :nickname]

  def new(%Pipo.Data.Player{} = player) do
    %__MODULE__{
      player_id: player.id,
      nickname: player.nickname,
      confirmed_play: false
    }
  end

  def confirm_play(meta) do
    %__MODULE__{meta | confirmed_play: true}
  end
end

defmodule Pipo.Data.Party do
  alias Pipo.Data.Party.PlayerMeta
  alias Pipo.Data.Board
  defstruct [:id, :status, :players_meta, :board, :player_turn, :winner]

  def get_players_ids(%__MODULE__{
        players_meta: players
      }) do
    Enum.map(players, & &1.player_id)
  end

  def has_player?(party, player_id) do
    party
    |> get_players_ids()
    |> Enum.member?(player_id)
  end

  def set_player_confirmed(party, player_id) do
    Map.update!(party, :players_meta, fn players_meta ->
      Enum.map(players_meta, fn pmeta ->
        if pmeta.player_id === player_id do
          PlayerMeta.confirm_play(pmeta)
        else
          pmeta
        end
      end)
    end)
  end

  def all_players_confirmed?(%__MODULE__{players_meta: pms}) do
    Enum.all?(pms, & &1.confirmed_play)
  end

  def start_game(%__MODULE__{} = party) do
    first_player = pick_random_player(party).player_id
    board = Board.new(3, 3)
    %__MODULE__{party | status: :awaiting_turn, player_turn: first_player, board: board}
  end

  def apply_turn(
        %__MODULE__{board: board, status: :awaiting_turn} = party,
        player_id,
        %{edge_type: edge_type, x: x, y: y} = edge
      ) do
    with :ok <- Board.validate_edge(board, edge),
         true <- Board.blank_edge?(board, edge) do
      board
      |> Board.set_edge(edge, player_id)
      |> Board.maybe_set_boxes(edge, player_id)
      |> case do
        {:unchanged, board} ->
          {:ok, %__MODULE__{party | board: board, player_turn: next_turn(party)}}

        {:changed, board} ->
          case Board.check_victory(board) do
            {:winner, winner} ->
              {:ok,
               %__MODULE__{
                 party
                 | board: board,
                   status: :finished,
                   winner: winner,
                   player_turn: nil
               }}

            :unfinished ->
              {:ok, %__MODULE__{party | board: board}}
          end
      end
    else
      false -> {:error, :not_blank}
      {:error, err} -> {:error, err}
    end
  end

  defp pick_random_player(%__MODULE__{players_meta: pms}) do
    Enum.random(pms)
  end

  defp next_turn(%__MODULE__{players_meta: pms, player_turn: current} = party) do
    party
    |> get_players_ids()
    |> next_turn(current)
  end

  defp next_turn(ids, current), do: next_turn(ids, current, hd(ids))

  defp next_turn([current | [next | _]], current, _), do: next

  defp next_turn([current | []], current, default), do: default

  defp next_turn([_ | others], current, default), do: next_turn(others, current, default)

  defp next_turn([_ | others], current, default), do: next_turn(others, current, default)
end

# defimpl Poison.Encoder, for: Pipo.Data.Party do
#   def encode(%Pipo.Data.Party{} = party, options) do
#     Poison.Encoder.Map.encode(party, options)
#   end
# end
