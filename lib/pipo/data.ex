defmodule Pipo.Data do
  alias Pipo.Data.Player
  alias Pipo.Data.Party
  alias Pipo.Data.Party.PlayerMeta
  require Logger
  @db Pipo.DB

  def create_player() do
    player_id = generate_player_id()

    player = %Pipo.Data.Player{
      id: player_id,
      nickname: player_id
    }

    :ok = save_model(player)
    player
  end

  def delete_party(store, party_id) do
    DevDB.delete(store, key_for(Party, party_id))
  end

  def delete_party_if_not_started(party_id, on_deleted) do
    :ok =
      DevDB.transaction(@db, fn repo ->
        case fetch_party(repo, party_id) do
          {:ok, %Party{status: :awaiting_confirm}} ->
            Logger.debug("Deleting unstarted party #{party_id}")
            :ok = delete_party(repo, party_id)
            on_deleted.()

          {:ok, %Party{status: _other}} ->
            :ok

          other ->
            raise "bad party data : #{inspect(other)}"
        end
      end)
  end

  def create_party(creator_id, other_players_ids) do
    {:ok, creator} = fetch_player(creator_id)

    creator_meta =
      creator
      |> PlayerMeta.new()
      |> PlayerMeta.confirm_play()

    others_meta =
      mget(Player, other_players_ids)
      |> Enum.map(&PlayerMeta.new/1)

    party_id = generate_party_id()

    party = %Pipo.Data.Party{
      id: party_id,
      status: :awaiting_confirm,
      players_meta: [creator_meta | others_meta]
    }

    :ok = save_model(party)

    # On party creation, we start a process to delete the party if not all
    # players confirm that they play in a maximum amount of time
    will_cancel_party_if_not_started(party_id)

    {:ok, party_id}
  end

  def party_has_player?(party_id, player_id) do
    case fetch_party(party_id) do
      {:ok, p} -> Party.has_player?(p, player_id)
      _ -> false
    end
  end

  def maybe_start_party(party_id) do
    DevDB.transaction(@db, fn store ->
      {:ok, party} = fetch_party(store, party_id)

      if Party.all_players_confirmed?(party) do
        started_party = Party.start_game(party)
        save_model(store, started_party)
        :commit
      else
        {:error, :not_started}
      end
    end)
    |> case do
      :ok -> :ok
      {:error, :not_started} -> :ok
      err -> err
    end
  end

  def confirm_player!(party_id, player_id) do
    :ok =
      DevDB.transaction(@db, fn store ->
        {:ok, party} = fetch_party(store, party_id)
        party = Party.set_player_confirmed(party, player_id)
        save_model(store, party)
        :commit
      end)
  end

  def submit_party_turn(player_id, party_id, %{edge_type: edge_type, x: x, y: y} = turn) do
    DevDB.transaction(@db, fn store ->
      case fetch_party(store, party_id) do
        :error ->
          {:error, :not_found}

        {:ok, party} ->
          if party.player_turn === player_id do
            case Party.apply_turn(party, player_id, turn) do
              {:ok, new_party} ->
                save_model(store, new_party)
                :commit

              {:error, err} ->
                {:error, err}
            end
          else
            {:error, :bad_turn}
          end
      end
    end)
  end

  def all_party_ids_with_player(player_id) do
    player_id
    |> all_parties_with_player()
    |> Enum.map(fn {_, %Party{id: id}} -> id end)
  end

  def all_parties_with_player(player_id) do
    {:ok, parties} = select_group(Party, &Party.has_player?(&1, player_id))
    parties
  end

  def player_exists?(key), do: exists?(Player, key)
  def party_exists?(key), do: exists?(Party, key)

  def exists?(group, key) do
    case DevDB.fetch(@db, key_for(group, key)) do
      {:ok, _} -> true
      _ -> false
    end
  end

  def mget(keys) do
    keys
    |> Stream.map(&DevDB.get(@db, &1, :not_found))
    |> Stream.filter(fn
      :not_found -> false
      _ -> true
    end)
  end

  def mget(group, keys) do
    keys
    |> Stream.map(&key_for(group, &1))
    |> mget()
  end

  def mget(group, keys, by_key)

  def mget(group, keys, true) do
    mget(group, keys)
    |> index_by_key()
  end

  def mget(group, keys, false) do
    mget(group, keys)
    |> Enum.to_list()
  end

  defp index_by_key(entries) do
    entries
    |> Stream.map(&{model_id(&1), &1})
    |> Enum.into(%{})
  end

  def fetch_party(store \\ @db, id), do: DevDB.fetch(store, key_for(Party, id))
  def fetch_player(id), do: DevDB.fetch(@db, key_for(Player, id))

  def select(store \\ @db, filter), do: DevDB.select(store, filter)

  def select_group(store \\ @db, group, filter)

  def select_group(store, group, filter) when is_function(filter, 2) do
    filter_group = fn
      val, {^group, key} -> filter.(val, key)
      _, _k -> false
    end

    select(store, filter_group)
  end

  def select_group(store, group, filter) when is_function(filter, 1) do
    select_group(store, group, fn val, _key -> filter.(val) end)
  end

  ## -- Helpers ---------------------------------------------------------------

  defp generate_player_id do
    :crypto.strong_rand_bytes(8)
    |> Base.url_encode64()
    |> binary_part(0, 8)
  end

  defp generate_party_id do
    UUID.uuid4()
  end

  defp save_model(store \\ @db, model) do
    key = key_for(model)
    DevDB.put(store, key, model)
  end

  defp key_for(%Player{id: id}), do: {Player, id}
  defp key_for(%Party{id: id}), do: {Party, id}
  defp key_for(group, id) when is_atom(group), do: {group, id}

  defp model_id(%Player{id: id}), do: id
  defp model_id(%Party{id: id}), do: id

  defp will_cancel_party_if_not_started(party_id) do
    Pipo.Task.CancelUnstartedParty.watch(party_id)
  end

  def cleanup_old_parties_on_start do
    {:ok, parties} = select_group(Party, fn _, _ -> true end)

    parties
    |> Enum.map(fn {_, %Party{id: id}} -> id end)
    |> Enum.map(&Pipo.Task.CancelUnstartedParty.watch(&1, 0))
  end
end

defmodule Pipo.Data.Codec do
  @behaviour DevDB.Codec
  def encode!(term, _), do: Macro.to_string(quote do: unquote(term))
  def decode!(string, _), do: Eon.from_string_unsafe!(string)
end
