defmodule Pipo.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(PipoWeb.Endpoint, []),
      supervisor(PipoWeb.Presence, []),
      {Task.Supervisor, name: Pipo.Task.Supervisor, restart: :transient},
      worker(DevDB, [
        [
          name: Pipo.DB,
          backend: Application.get_env(:pipo, Pipo.DB)[:backend].(),
          seed: :backend
        ]
      ])
    ]

    opts = [strategy: :one_for_one, name: Pipo.Supervisor]
    {:ok, pid} = Supervisor.start_link(children, opts)

    spawn(fn ->
      Process.sleep(1000)
      Pipo.Data.cleanup_old_parties_on_start()
    end)

    {:ok, pid}
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PipoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
