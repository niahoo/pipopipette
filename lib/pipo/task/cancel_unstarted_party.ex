defmodule Ms do
  # Express everything in milliseconds
  def milliseconds(n), do: n

  def second(n), do: seconds(n)
  def seconds(n), do: milliseconds(n * 1000)

  def minute(n), do: minutes(n)
  def minutes(n), do: seconds(n * 60)

  def hour(n), do: hours(n)
  def hours(n), do: minutes(n * 60)
end

defmodule Pipo.Task.CancelUnstartedParty do
  use Task, restart: :transient
  alias Pipo.Data
  @sup Pipo.Task.Supervisor
  require Ms

  # @party_accept_timeout Ms.minutes(5)
  @party_accept_timeout Ms.seconds(30)

  def watch(party_id, timeout \\ @party_accept_timeout) do
    task_opts = [restart: :transient]
    Task.Supervisor.start_child(@sup, __MODULE__, :run, [party_id, timeout], task_opts)
  end

  def run(party_id, timeout) do
    # First we sleep to let the players accept the party, and then we check and
    # maybe delete the party.
    Process.sleep(timeout)

    Data.delete_party_if_not_started(party_id, fn ->
      topic = PipoWeb.PlayerChannel.party_topic(party_id)

      PipoWeb.Endpoint.broadcast!(topic, "party_canceled", %{
        "party_id" => party_id
      })
    end)
  end
end
