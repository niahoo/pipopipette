defmodule PipoWeb.Presence do
  use Phoenix.Presence,
    otp_app: :pipo,
    pubsub_server: Pipo.PubSub

  def fetch(_topic, entries) do
    player_ids = Map.keys(entries)

    data = Pipo.Data.mget(Pipo.Data.Player, player_ids, true)

    for {player_id, %{metas: metas}} <- entries, into: %{} do
      {
        player_id,
        %{metas: metas, player: Map.get(data, player_id, %{})}
      }
    end
  end
end
