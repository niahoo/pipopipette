defmodule PipoWeb.LobbyChannel do
  use PipoWeb, :channel

  def join("lobby:main", payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (lobby:main).
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    push(socket, "presence_state", PipoWeb.Presence.list(socket))

    {:ok, _} =
      PipoWeb.Presence.track(socket, socket.assigns.player_id, %{
        online_at: System.system_time(:seconds)
      })

    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    IO.puts("CHANNEL LEFT")
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
