defmodule PipoWeb.PlayerChannel do
  use PipoWeb, :channel
  # party_invited is on the main topic player:<player_id> so the broadcast is
  # sent down the socket if not intercepted. Broadcasts to party:<party_id>
  # topics are caught in handle_info
  intercept(["party_invited"])

  alias Pipo.Data
  alias Pipo.Data.Party
  alias Pipo.Data.Player

  alias Phoenix.Socket.Broadcast
  def party_topic(party_id), do: "party:#{party_id}"

  def join("player:" <> player_id, _payload, socket) do
    if player_id === socket.assigns.player_id do
      parties_ids =
        player_id
        |> player_current_parties_ids()

      socket =
        socket
        |> assign(:topics, [])
        |> subscribe_to_parties(parties_ids)

      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  @todo "Check if player already has a party with this oponent"
  def handle_in("party_create", %{"oponent_id" => oponent_id}, socket) do
    player_id = socket.assigns.player_id

    with true <- Data.player_exists?(oponent_id),
         {:ok, party_id} <- Data.create_party(player_id, [oponent_id]) do
      socket = subscribe_to_party(socket, party_id)
      :ok = subscribe_other_player_to_party(oponent_id, party_id)
      {:reply, {:ok, %{party_id: party_id}}, socket}
    else
      false -> {:error, "Player does not exist"}
      {:error, reason} -> {:error, reason}
    end
  end

  def handle_in("party_accept", %{"party_id" => party_id}, socket) do
    player_id = socket.assigns.player_id

    with true <- Data.party_exists?(party_id),
         true <- Data.party_has_player?(party_id, player_id) do
      :ok = Data.confirm_player!(party_id, player_id)
      :ok = Data.maybe_start_party(party_id)
      broadcast_party(party_id)
    end

    {:noreply, socket}
  end

  def handle_in("party_turn", %{"party_id" => party_id, "turn" => turn}, socket) do
    player_id = socket.assigns.player_id
    %{"edge_type" => edge_type, "x" => x, "y" => y} = turn
    turn = %{edge_type: edge_type, x: x, y: y}

    case Data.submit_party_turn(player_id, party_id, turn) do
      :ok ->
        broadcast_party(party_id)
        {:reply, :ok, socket}

      {:error, err} ->
        {:reply, {:ok, %{error: err}}, socket}
    end
  end

  # This is called from another player's channel who created the party.
  def handle_out("party_invited", %{"party_id" => party_id}, socket) do
    player_id = socket.assigns.player_id
    true = Data.party_has_player?(party_id, player_id)
    socket = subscribe_to_party(socket, party_id)
    {:noreply, socket}
  end

  def handle_out("party_canceled", %{"party_id" => party_id}, socket) do
    player_id = socket.assigns.player_id

    socket =
      if Data.party_has_player?(party_id, player_id) do
        subscribe_to_party(socket, party_id)
      end

    {:noreply, socket}
  end

  def handle_info({:subscribed, {:party, party_id}}, socket) do
    broadcast_party(party_id)
    {:noreply, socket}
  end

  def handle_info(%Broadcast{topic: "party:" <> party_id} = bcast, socket) do
    %Broadcast{event: ev, payload: payload} = bcast

    case handle_party_broadcast(ev, party_id, payload, socket) do
      {:reply, reply, socket} ->
        {:reply, reply, socket}

      {:noreply, socket} ->
        {:noreply, socket}

      other ->
        raise "handle_party_broadcast must return a channel reply"
    end
  end

  def handle_info(info, socket) do
    IO.puts("\nUNHANDLED INFO #{inspect(info)}\n")
    {:noreply, socket}
  end

  def handle_party_broadcast(ev, _party_id, payload, socket)
      when ev in ["party_update", "party_canceled"] do
    push(socket, ev, payload)
    {:noreply, socket}
  end

  defp player_current_parties_ids(player_id) do
    Data.all_party_ids_with_player(player_id)
  end

  # When we subscribe to a party, we should immediately push the party state to
  # the player, so we send self() a msg to do so
  defp subscribe_to_party(socket, party_id) do
    topic = party_topic(party_id)
    topics = socket.assigns.topics

    if topic in topics do
      # party already subsribed, do nothing
      socket
    else
      :ok = PipoWeb.Endpoint.subscribe(topic)
      send(self(), {:subscribed, {:party, party_id}})
      assign(socket, :topics, [topic | topics])
    end
  end

  defp subscribe_to_parties(socket, parties_ids) do
    parties_ids
    |> Enum.reduce(socket, fn party_id, acc ->
      subscribe_to_party(acc, party_id)
    end)
  end

  defp subscribe_other_player_to_party(player_id, party_id) do
    topic = "player:" <> player_id

    PipoWeb.Endpoint.broadcast!(topic, "party_invited", %{
      "party_id" => party_id
    })
  end

  defp broadcast_party(party_id) do
    case Data.fetch_party(party_id) do
      {:ok, party} ->
        topic = party_topic(party_id)

        PipoWeb.Endpoint.broadcast!(topic, "party_update", %{
          "party_id" => party_id,
          "party" => party
        })

      :error ->
        # this is really bad because the party should exist. Or the user reload
        # the brower page just as the party is terminating maybe ... just ignore
        IO.puts("@todo why broadcast unknown party ?")
        :pass
    end
  end

  defp subscribe_other_player_to_party(player_id, party_id) do
    topic = "player:" <> player_id

    PipoWeb.Endpoint.broadcast!(topic, "party_invited", %{
      "party_id" => party_id
    })
  end
end
