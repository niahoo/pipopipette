defmodule PipoWeb.PageController do
  use PipoWeb, :controller
  alias Pipo.Data
  plug(:set_player_id)

  def index(conn, _params) do
    player_id = get_session(conn, :player_id)
    render(conn, "index.html", %{player_id: player_id})
  end

  defp set_player_id(conn, _params) do
    get_new_user_id = fn ->
      Data.create_player().id
    end

    case get_session(conn, :player_id) do
      nil ->
        put_session(conn, :player_id, get_new_user_id.())

      player_id ->
        if Data.player_exists?(player_id) do
          IO.puts("PLAYER ALREADY EXISTS")
          conn
        else
          player_id = get_new_user_id.()
          IO.puts("CREATED PLAYER #{player_id}")
          put_session(conn, :player_id, player_id)
        end
    end
  end
end
