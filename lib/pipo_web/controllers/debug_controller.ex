defmodule PipoWeb.DebugController do
  use PipoWeb, :controller
  alias Pipo.Data.Party
  alias Pipo.Data.Player

  plug(:put_view, PipoWeb.DataView)

  def dropdb(conn, _params) do
    {:ok, result} =
      DevDB.transaction(Pipo.DB, fn db ->
        {:ok, rows} = DevDB.select(db, fn _, _ -> true end)

        deleted_count =
          rows
          |> Enum.reduce(0, fn {k, v}, acc ->
            case k do
              {Player, player_id} ->
                PipoWeb.Endpoint.broadcast("user_socket:#{player_id}", "disconnect", %{})

              _ ->
                :ok
            end

            :ok = DevDB.delete(db, k)
            acc + 1
          end)

        {:ok, deleted_count}
      end)

    render(conn, "show.json", %{data: result})
  end
end
