defmodule PipoWeb.PartyController do
  use PipoWeb, :controller
  alias Pipo.Data
  alias Pipo.Data.Party

  plug(:put_view, PipoWeb.DataView)

  def parties(conn, _params) do
    parties = Data.all_parties()
    render(conn, "show.json", %{data: parties})
  end

  def my_parties(conn, _params) do
    player_id = get_session(conn, :player_id)

    parties =
      Data.all_parties_with_player(player_id)
      |> map_by_key()

    render(conn, "show.json", %{data: parties})
  end

  def my_parties_ids(conn, _params) do
    player_id = get_session(conn, :player_id)

    ids =
      player_id
      |> Party.all_ids_with_player()

    render(conn, "show.json", %{data: ids})
  end

  defp all_parties() do
    {:ok, parties} = Data.select_group(Party, fn _, _ -> true end)
    map_by_key(parties)
  end

  defp map_by_key(parties) do
    parties
    |> Stream.map(fn {{Party, id}, party} ->
      {id, party}
    end)
    |> Enum.into(%{})
  end
end
