defmodule PipoWeb.Router do
  use PipoWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", PipoWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/debug/dropdb", DebugController, :dropdb)
    get("/parties", PartyController, :parties)
    get("/parties/mine", PartyController, :my_parties)
    get("/parties/mine/ids", PartyController, :my_parties_ids)
    get("/", PageController, :index)
    get("/party/*party_id_ignored", PageController, :index)
  end

  # Other scopes may use custom stacks.
  # scope "/api", PipoWeb do
  #   pipe_through :api
  # end
end
