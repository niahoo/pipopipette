defmodule PipoWeb.DataView do
  use PipoWeb, :view

  def render("show.json", %{data: data}) do
    # Always wrap json data in an object for some reason I forgot
    %{data: data}
  end
end
